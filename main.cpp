#include <iostream>
#include <algorithm>

using namespace std;

class hash
{
private:
    int *vetor,tamanho,ocp;
    float carga;
    char *ocupado;

    void aumentaHash ()
    {
        cout << " ::: Aumento ::: " << endl;
        getStatus();
        cout << " ::: Tabela  :::" << endl;
        printHash();
        cout << endl;
        int aux[tamanho],i;
        char auxoc[tamanho];
        copy(&vetor[0],&vetor[tamanho],&aux[0]);
        copy(&ocupado[0],&ocupado[tamanho],&auxoc[0]);
        tamanho*=2;
        vetor = new int[tamanho];
        ocupado = new char[tamanho];
        ocp = 0;
        fill(&ocupado[0],&ocupado[tamanho],0);
        for (i=0;i<tamanho/2;i++)
            if (auxoc[i]==1)
                insValor(aux[i]);
        delete[] aux;
        delete[] auxoc;


    }
public:
    hash ()
    {
        tamanho = 2;
        ocp=0;
        carga=0;
        vetor = new int[tamanho];
        ocupado = new char[tamanho];
        fill(&ocupado[0],&ocupado[tamanho],0);
    }
    void insValor(int val)
    {
        int pos;
        pos = val%tamanho;
        while (ocupado[pos]==1)
            pos++;
        vetor[pos] = val;
        ocupado[pos]=1;
        ocp++;
        carga = (float)ocp/(float)tamanho;
        if (carga > 0.75)
            aumentaHash();

    }
    void buscaValor (int val)
    {
        int pos = val%tamanho;
        while (ocupado[pos]==1)
        {
            if (vetor[pos]==val)
            {
                cout << "Valor encontrado na posicao " << pos << endl;
                return;
            }
            else
                pos++;
        }
        cout << "Valor nao encontrado." << endl;
        return;
    }
    void printHash ()
    {
        int i;
        for (i=0;i<tamanho;i++)
            if (ocupado[i]==1)
                cout << "pos " << i << ": " <<vetor[i] << endl;
    }
    void getStatus ()
    {
        cout << "Tamanho:" << tamanho << endl;
        cout << "Ocp:" << ocp << endl;
        cout << "Carga:" << carga << endl;
    }
};

int main()
{
    hash tabela;
    tabela.insValor(10);
    tabela.insValor(20);
    tabela.buscaValor(1000);
    tabela.insValor(40);
    tabela.insValor(80);
    tabela.insValor(1000);
    tabela.buscaValor(1000);
    cout << " ::: Final   :::" << endl;
    tabela.getStatus();
    cout << " ::: Tabela  :::" << endl;
    tabela.printHash();
    return 0;
}
